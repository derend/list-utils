#lang scribble/manual
@(require scribble/eval
          racket/sandbox
          "main.rkt"
          (for-label racket
                     "main.rkt"
                     )
          )
@(define (author-email) "deren.dohoda@gmail.com")

@title[#:tag "top"]{Additional List Utilities}
@author{@(author+email "Deren Dohoda" (author-email))}

@(define eval (parameterize ((sandbox-output 'string)
                             (sandbox-error-output 'string)
                             (sandbox-memory-limit 5))
                (make-evaluator 'racket/base #:requires '("main.rkt"))))

@defmodule[list-utils]

@table-of-contents[]

@section{List Size}
@deftogether[(@defproc[(length>? [l list?]
                                 [n (>/c 0)])
                       boolean?]
               @defproc[(length>=? [l list?]
                                   [n (>/c 0)])
                        boolean?]
               @defproc[(length<? [l list?]
                                  [n (>/c 0)])
                        boolean?]
               @defproc[(length<=? [l list?]
                                   [n (>/c 0)])
                        boolean?]
               @defproc[(length=? [l list?]
                                  [n (>/c 0)])
                        boolean?])]{Checks that the list @italic{l} satisfies the named predicate with a given size @italic{n} without walking the whole list if it isn't necessary.}

@section{Safe Manipulation}
@defproc[(safe-take [l list?]
                    [n (>=/c 0)])
         list?]{Attempts to take @italic{n} elements from the list @italic{l}, but will yield a shorter list if @italic{l} runs out of terms.}
@defproc[(safe-drop [l list?]
                    [n (>=/c 0)])
         list?]{Attempts to drop @italic{n} elements from the list @italic{l}, but will yield an empty list if @italic{l} runs out of terms.}
@defproc[(safe-map [proc procedure?]
                   [l list?] ...)
         list?]{Attempts to map the procedure @italic{proc} to lists @italic{l ...} like @racket[map], but if any list runs out of terms the entire map finishes at that point.}
@deftogether[(@defproc[(third... [l list?]) list?]
               @defproc[(fourth... [l list?]) list?]
               @defproc[(fifth... [l list?]) list?]
               @defproc[(sixth... [l list?]) list?])]{Built-ins for @racket{safe-drop}, meaning they may yield empty lists if there are not enough entries.}

@section{List Lengths}
;list-from-to list-replace force-length
@defproc[(list-from-to [from exact-integer?]
                       [to exact-integer?]
                       [#:by inc exact-integer? 1])
         list?]{Creates a list incrementing between @italic{from} and @italic{to} inclusive with step size @italic{inc}. Will automatically increase or decrease as needed.}
@defproc[(list-replace [l list?]
                       [pos exact-nonnegative-integer?]
                       [sub any/c?])
         list?]{Replacement element of a list at position @italic{pos} with @italic{sub}. It is an error to specify a position that isn't in the list.}
@defproc[(force-length [len exact-positive-integer?]
                       [l list?]
                       [#:default val any/c? 0])
         list?]{Truncate a list @italic{l} to @italic{len}, or grow a list to length @italic{len} using @italic{val} as entries.}

@section{Destructing Simple Lists}
@defform[(destructure list (id ...) body)]{Binds the values of @italic{list} to the names @italic{id}s in order of appearance in both for use in the @italic{body}.
 If the quantity of @italic{id}s provided is longer than the quantity of entries in the given list, those @italic{id}s will simply be @racket[null].}

@section{Filtering}
@defproc[(filter* [procs (listof procedure?)]
                  [l (list?)])
         list?]{Filters the values of @italic{l} such that each element satisfies each of the @italic{procs}.}
@defproc[(filter-values [proc procedure?] [ls list?] ...) list?]{Separately filters each list in @italic{ls ...} with @italic{proc} like @racket[filter], but yields the resulting filtered lists as @racket[values].}
@defproc[(filter-on [proc procedure?] [acc procedure?] [l list?]) list?]{Like Racket @racket[filter], but for lists whose elements are themselves accessed through a procedure @italic{acc}.}
@examples[#:eval eval
          (filter-on even?
                     cdr
                     (map cons (build-list 5 values)
                          (map add1 (build-list 5 values))))]